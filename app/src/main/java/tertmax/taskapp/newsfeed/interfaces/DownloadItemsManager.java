package tertmax.taskapp.newsfeed.interfaces;

import java.util.List;

import tertmax.taskapp.newsfeed.model.NewsItem;


public interface DownloadItemsManager {

    List<NewsItem> downloadItems(MainPresenter presenter);

}
