package tertmax.taskapp.newsfeed.interfaces;


import java.util.List;

import tertmax.taskapp.newsfeed.model.NewsItem;

public interface MainPresenter {
    public static final String NEWS_ITEM_URL = "item_url";

    void onResume();

    void onDestroy();

    void setSnackbar();

    void onDownloadItems();

    boolean isOnline();

    void onFinishDownload(List<NewsItem> items);

    void sendMessage(String message);
}
