package tertmax.taskapp.newsfeed.interfaces;

import java.util.List;

import tertmax.taskapp.newsfeed.model.NewsItem;

public interface MainView {

    void setItems(List<NewsItem> items);

    void showSnackbar();

    void hideSnackbar();

    void showErrorMessge(String message);

}
