package tertmax.taskapp.newsfeed.presenter;

import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tertmax.taskapp.newsfeed.api.TimesofIndiaAPI;
import tertmax.taskapp.newsfeed.interfaces.DownloadItemsManager;
import tertmax.taskapp.newsfeed.interfaces.MainPresenter;
import tertmax.taskapp.newsfeed.model.NewsItem;
import tertmax.taskapp.newsfeed.model.NewsList;

public class DownloadItemsManagerImplementation implements DownloadItemsManager {

    private TimesofIndiaAPI mAPIService;
    private NewsList mNewsList;

    @Override
    public List<NewsItem> downloadItems(final MainPresenter presenter) {
        loadNews(presenter);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            presenter.onFinishDownload(getNews());
            }
        }, 2000);

        return getNews();

    }

    private List<NewsItem> getNews() {
        if (mNewsList != null) {
            return mNewsList.items;
        }
        return new ArrayList<NewsItem>();
    }

   public TimesofIndiaAPI getTimesofIndiaAPI() {
        if (mAPIService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(TimesofIndiaAPI.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mAPIService = retrofit.create(TimesofIndiaAPI.class);
        }
        return mAPIService;
    }

    private void loadNews(final MainPresenter presenter) {
        Call<NewsList> listCall = getTimesofIndiaAPI().getNewsItems();
        listCall.enqueue(new Callback<NewsList>() {
            @Override
            public void onResponse(Call<NewsList> call, Response<NewsList> response) {
                if (response.isSuccessful()) {
                    mNewsList = response.body();
                } else {
                    int sc = response.code();
                    switch (sc) {
                    }
                }
            }

            @Override
            public void onFailure(Call<NewsList> call, Throwable t) {
                presenter.sendMessage(t.getMessage());
            }
        });

    }
}