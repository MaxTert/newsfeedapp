package tertmax.taskapp.newsfeed.presenter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.List;

import tertmax.taskapp.newsfeed.interfaces.DownloadItemsManager;
import tertmax.taskapp.newsfeed.interfaces.MainPresenter;
import tertmax.taskapp.newsfeed.interfaces.MainView;
import tertmax.taskapp.newsfeed.model.NewsItem;

public class NewsPresenter implements MainPresenter {

    private Context mContext;
    private MainView mMainView;
    private DownloadItemsManager mDownloadItemsManager;


    public NewsPresenter(MainView mainView, DownloadItemsManager downloadItemsManager,
                         Context context) {
        mContext = context;
        mMainView = mainView;
        mDownloadItemsManager = downloadItemsManager;
    }

    @Override
    public void onDownloadItems() {
         mDownloadItemsManager.downloadItems(this);
    }

    @Override
    public void onFinishDownload(List<NewsItem> news){
        if (mMainView != null) {
            mMainView.setItems(news);
        }
    }

    @Override
    public void sendMessage(String message) {
        mMainView.showErrorMessge(message);
    }

    @Override
    public void onResume() {
        onDownloadItems();
    }

    @Override
    public void onDestroy() {
        mMainView = null;
    }

    @Override
    public void setSnackbar() {
        if (!isOnline()) {
            mMainView.showSnackbar();
        } else {
            mMainView.hideSnackbar();
        }

    }
    @Override
    public boolean isOnline(){
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


}

