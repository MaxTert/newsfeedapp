package tertmax.taskapp.newsfeed.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsItem {

    @SerializedName("HeadLine")
    private String headLine;
    @SerializedName("DateLine")
    private String dateLine;
    @SerializedName("WebURL")
    private String webURL;
    @SerializedName("Image")
    private ItemImage image;


    public String getHeadLine() {
        return headLine;
    }

    public String getDateLine() {
        String[] dateAndTime = dateLine.split(",");
        return dateAndTime[0] + ","+ dateAndTime[1];
    }
    public String getTimeLine(){
        String[] dateAndTime = dateLine.split(",");
        String time = dateAndTime[2];
        time = time.replace(" IST","");
        time = time.replace(".", ":");
        time = time.substring(0,6) + " " + time.substring(6);
        return time;
    }

    public String getWebURL() {
        return webURL;
    }

    public String getImageThumb() {
        return image.getThumb();
    }

    public String getImage() {
        return image.getPhoto();
    }


}

class ItemImage {

    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("Thumb")
    @Expose
    private String thumb;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

}
