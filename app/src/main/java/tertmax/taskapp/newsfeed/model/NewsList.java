package tertmax.taskapp.newsfeed.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsList {

    @SerializedName("NewsItem")
    public List<NewsItem> items;

}
