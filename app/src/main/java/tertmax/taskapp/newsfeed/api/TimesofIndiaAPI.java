package tertmax.taskapp.newsfeed.api;

import retrofit2.Call;
import retrofit2.http.GET;
import tertmax.taskapp.newsfeed.model.NewsList;

public interface TimesofIndiaAPI {

    public static final String BASE_URL = "http://timesofindia.indiatimes.com/";

    @GET("feeds/newsdefaultfeeds.cms?feedtype=sjson")
    Call<NewsList> getNewsItems();


}
