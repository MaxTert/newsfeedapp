package tertmax.taskapp.newsfeed.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import tertmax.taskapp.newsfeed.R;
import tertmax.taskapp.newsfeed.activities.adapter.NewsRVAdapter;
import tertmax.taskapp.newsfeed.activities.decoration.GridItemSpaceDecoration;
import tertmax.taskapp.newsfeed.interfaces.MainPresenter;
import tertmax.taskapp.newsfeed.interfaces.MainView;
import tertmax.taskapp.newsfeed.model.NewsItem;
import tertmax.taskapp.newsfeed.presenter.DownloadItemsManagerImplementation;
import tertmax.taskapp.newsfeed.presenter.NewsPresenter;

public class NewsFeedActivity extends AppCompatActivity implements MainView,
        SwipeRefreshLayout.OnRefreshListener {

    protected static MainPresenter mPresenter;
    private RecyclerView mRecyclerViewNews;
    private NewsRVAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Snackbar mSnackbar;
    private boolean mIsUserNotified;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_feed);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh_news_feed);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mIsUserNotified = false;

        mRecyclerViewNews = (RecyclerView) findViewById(R.id.recyclerview_news_feed);
        initializeList();
        initializeSnackbar();

        mPresenter = new NewsPresenter(this, new DownloadItemsManagerImplementation(),
                getApplicationContext());
    }

    private void initializeList() {
        mAdapter = new NewsRVAdapter(getBaseContext());
        mAdapter.setItemClickListener(new NewsRVAdapter.ItemClickListener() {
            @Override
            public void onItemClick(NewsItem newsItem) {
                Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
                intent.putExtra(MainPresenter.NEWS_ITEM_URL, newsItem.getWebURL());
                startActivity(intent);
            }
        });
        mRecyclerViewNews.setHasFixedSize(true);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.item_offset);
        mRecyclerViewNews.setAdapter(mAdapter);
        if (getBaseContext().getResources().getConfiguration().
                orientation == Configuration.ORIENTATION_PORTRAIT) {
            mRecyclerViewNews.addItemDecoration(new GridItemSpaceDecoration(2, spacingInPixels, true));
            mRecyclerViewNews.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2,
                    LinearLayoutManager.VERTICAL, false));
        } else {
            mRecyclerViewNews.addItemDecoration(new GridItemSpaceDecoration(3, spacingInPixels, true));
            mRecyclerViewNews.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3,
                    LinearLayoutManager.VERTICAL, false));
        }
    }
    private void initializeSnackbar(){
        mSnackbar = Snackbar
                .make(mSwipeRefreshLayout,
                        R.string.string_snackbar_noconnection,
                        Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.string_snackbar_retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       onRefresh();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        onRefresh();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void setItems(List<NewsItem> items) {
        mPresenter.setSnackbar();
        mAdapter.setNewsItemsList(items);
    }

    @Override
    public void showSnackbar() {
        mSnackbar.show();
    }

    @Override
    public void hideSnackbar() {
        mSnackbar.dismiss();
    }

    @Override
    public void showErrorMessge(String message) {
        if(!mIsUserNotified) {
            AlertDialog.Builder builder = new AlertDialog.Builder(NewsFeedActivity.this);
            builder.setTitle(R.string.string_error_title)
                    .setMessage(message)
                    .setCancelable(false)
                    .setNegativeButton(R.string.string_ok_error,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        mIsUserNotified = true;
        }
    }

    @Override
    public void onRefresh() {

            mSwipeRefreshLayout.setRefreshing(true);
            mSwipeRefreshLayout.postDelayed(new Runnable() {

                @Override
                public void run() {
                        mPresenter.onResume();
                    if(mPresenter.isOnline()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        mIsUserNotified = false;
                    }else{
                        onRefresh();
                    }
                }
            }, 2000);


    }

}
