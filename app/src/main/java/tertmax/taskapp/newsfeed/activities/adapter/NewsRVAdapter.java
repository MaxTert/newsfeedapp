package tertmax.taskapp.newsfeed.activities.adapter;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import tertmax.taskapp.newsfeed.R;
import tertmax.taskapp.newsfeed.model.NewsItem;

public class NewsRVAdapter extends RecyclerView.Adapter<NewsRVAdapter.NewsViewHolder>{


    private List<NewsItem> mNewsItemsList;
    private ItemClickListener mItemClickListener;
    private Context mContext;



    public NewsRVAdapter(Context context){
        mNewsItemsList = new ArrayList<>();
        mContext = context;
    }

    public void setItemClickListener(ItemClickListener clickListener){
        mItemClickListener = clickListener;
    }

    public void setNewsItemsList(List<NewsItem> list){
        mNewsItemsList = list;
        notifyDataSetChanged();
    }


    public interface ItemClickListener{
        void onItemClick(NewsItem newsItem);
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_recyclerview_item, parent, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mItemClickListener != null){
                    Object tag = v.getTag();
                    if(tag instanceof Integer){
                        mItemClickListener.onItemClick(mNewsItemsList.get((Integer) tag));
                    }
                }
            }
        });
        return  new NewsViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {

        Picasso.with(mContext)
                .load(mNewsItemsList.get(position).getImage())
                .into(holder.photoImageView);
        holder.headlineTextView.setText(mNewsItemsList.get(position).getHeadLine());
        holder.datelineTextView.setText(mNewsItemsList.get(position).getDateLine());
        holder.timelineTextView.setText(mNewsItemsList.get(position).getTimeLine());
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mNewsItemsList.size();
    }

    public static class NewsViewHolder extends RecyclerView.ViewHolder{

        CardView newsCardView;
        ImageView photoImageView;
        TextView headlineTextView;
        TextView datelineTextView;
        TextView timelineTextView;


        public NewsViewHolder(View itemView) {
            super(itemView);
            newsCardView = (CardView) itemView.findViewById(R.id.cardview_item);
            photoImageView = (ImageView) itemView.findViewById(R.id.imageview_item_photo);
            headlineTextView = (TextView) itemView.findViewById(R.id.textview_item_headline);
            datelineTextView = (TextView) itemView.findViewById(R.id.textview_item_date);
            timelineTextView = (TextView) itemView.findViewById(R.id.textview_item_time);

        }

    }

}
